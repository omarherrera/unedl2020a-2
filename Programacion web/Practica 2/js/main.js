$(document).ready(init);
function init() {
	$.fn
		.scrollPath("getPath")
		.moveTo(400, 400, { name: "section1" })
		.lineTo(1040, 400)
		.lineTo(1040, 1120, { name: "section2" })
		.lineTo(420, 2100, { name: "section3", rotate: Math.PI / 2 })
		.lineTo(2220, 2800, { name: "section4", rotate: 0 })
		.lineTo(2220, 620)
		.arc(2000, 620, 220, 0, -Math.PI / 2, true)
		.lineTo(400, 400);
	$("#cont").scrollPath({
		drawPath: false,
		wrapAround: true,
		scrollBar: false,
	});
	$("nav")
		.find("a")
		.each(function () {
			var target = $(this).attr("href").replace("#", "");
			$(this).click(function (e) {
				e.preventDefault();
				$.fn.scrollPath("scrollTo", target, 1000, "easeInOutSine");
			});
		});
}
