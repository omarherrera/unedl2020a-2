$(document).ready(revista);
function revista() {
  $("#revista").booklet({
    width: 800,
    height: 600,

    closed: true,
    cover: true,
    autoCenter: true,
    hoverWidth: 100,
  });

  $(".bt_go").click(function () {
    $("#revista").booklet("gotopage", $(".in_go").val());
  });
}
