%macro escribir 2
mov eax, 4
mov ebx, 1
mov ecx, %1
mov edx, %2
int 0x80
%endmacro

section .data
msg db "Ingrese el valor de num 1: ",0xA
len equ $ - msg

msg2 db "Ingrese el valor del num 2: ",0xA
len2 equ $ - msg2

salto db "",0xA
lenSalto equ $ - salto


section .bss
num1 resb 2
num2 resb 2
res resb 1
section .text

leer:
	mov eax, 3
	mov ebx, 0
	mov edx, 2
	int 0x80
ret

        global _start
_start:

    escribir msg,len
    mov ecx, num1
    call leer
    
    escribir salto,lenSalto
    
    escribir msg2,len2
    mov ecx,num2
    call leer
    escribir salto,lenSalto
    
    mov ah,[num1]
    sub ah,'0'
    mov bh,[num2]
    sub bh,'0'
    
    cmp ah,bh
    jne diferente
    
    mov al,[num1]
    sub al,'0'
    add al,1
    add al,'0'
    mov [res],al
    escribir res,1
    jmp salida 
    
diferente:
    mov al,[num1]
    sub al,'0'
    sub al,1
    add al,'0'
    mov [res],al
    escribir res,1
    jmp salida

salida:
    mov eax,1
    int 0x80