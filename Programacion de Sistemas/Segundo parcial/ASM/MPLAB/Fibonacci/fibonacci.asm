;Fibonacci

	LIST P = 16F84A
	INCLUDE <P16F84A.INC>

	CBLOCK 0x0C
	Penultimo
	Ultimo
	Suma
	ENDC

	ORG 0
Inicio
	bsf STATUS,RP0
	clrf TRISB
	bcf STATUS,RP0
Principal
	clrf Penultimo
	movlw .1
Sumando
	movwf Ultimo
	addwf Penultimo,W
	movwf Suma
	btfsc STATUS,C
	goto Resul
	movf Ultimo,W
	movwf Penultimo
	movf Suma,W
	goto Sumando
Resul
	movfw Ultimo
	movwf PORTB
Salida
	sleep
	END