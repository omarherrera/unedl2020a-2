;Compara el dato de PORTA con un NUMERO d'10
;Si el si PORTA es mayor que numero, se encienden todos los leds
;Si el PORTA es menor o igual que NUMERO se encienden solo los pares
;zona de datos
LIST P = 16F84A
INCLUDE<P16F84A.INC>
NUMERO EQU d'10'

	ORG 0

;Zona de codigo
Inicio
	bsf STATUS,RP0		;En el banco 1 se configuran entradas y salidas, en el 0 empiezan las instrucciones
	clrf PORTB
	movlw b'00011111'
	movwf PORTA
	bcf STATUS,RP0
Principal
	movlw NUMERO		
	subwf PORTA,W
	movlw 	b'01010101'
	btfsc STATUS,C
	movlw b'11111111'
	movwf PORTB
	goto Principal
	END