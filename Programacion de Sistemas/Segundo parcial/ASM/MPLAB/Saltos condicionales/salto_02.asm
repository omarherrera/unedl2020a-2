;Comparar el dato del PORTA y un numero
;Si el numero y porta son iguales prenden todos los leds si no, solo los pares
;Zona de datos	
	LIST P = 16F84A
	INCLUDE <P16F84A.INC>
	NUMERO EQU d'10'
	ORG 0

;Zona de codigo
Inicio
	bsf STATUS,RP0
	clrf PORTB
	movlw b'00011111'
	movwf PORTA
	bcf STATUS,RP0

Principal
	movlw NUMERO
	subwf PORTA,W
	;Si la resta es 0, C = 1 y Z = 1
	;Si la resta es 1, C = 1 y Z = 0
	movlw b'11111111'
	btfss STATUS,Z
	movlw b'01010101'
	movwf PORTB
	goto Principal
	END