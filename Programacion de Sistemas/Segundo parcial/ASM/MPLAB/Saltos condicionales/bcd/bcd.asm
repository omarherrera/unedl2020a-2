LIST P = 16F84A
INCLUDE <P16F84A.INC>
NUMERO EQU d'124'
	
	CBLOCK 0x0C
	Centenas
	Decenas
	Unidades
	ENDC

	ORG 0

Inicio
	bsf STATUS,RP0
	clrf PORTB
	bcf STATUS,RP0
Principal
	clrf Centenas
	clrf Decenas
	movlw NUMERO
	movwf Unidades
Resta_Unidades
	movlw .10
	subwf Unidades,W
	btfss STATUS,C
	goto Salida
Incrementar_Decenas
	movwf Unidades
	incf Decenas,F
	movlw .10
	subwf Decenas,W
	btfss STATUS,C
	goto Resta_Unidades
Incrementar_Centenas
	clrf Decenas
	incf Centenas,F
	goto Resta_Unidades
Salida
	swapf	Decenas,W
	addwf	Unidades,W
	movwf	PORTB
	sleep

	END