;Comparar PORTA con un NUMERO 
;PORTA > NUMERO -- C=1 Z=0 prende los pares
;PORTA < NUMERO -- C=0 Z=0 prende Nibble alto
;PORTA = NUMERO -- C=1 Z=1 prende todos los leds

	LIST P=16F84A
	INCLUDE <P16F84A.INC>

NUMERO EQU d'10'
	ORG 0
Inicio
	bsf STATUS,RP0
	clrf PORTB
	movlw b'00011111'
	movwf PORTA
	bcf STATUS,RP0
Principal
	movlw NUMERO
	subwf PORTA,W		;Resultado de la resta afecta banderas
	movlw b'11110000'	;Suponiendo que PORTA es menor que NUMERO
	;si, si C=0 Y Z=0
	btfss STATUS,C
	;termine el programa y muestre en PORTB
	goto Salida
	movlw b'11111111'	;Son iguales
	btfsc STATUS,Z
	goto Salida		;Si Z=1 emovjecuta a salida
	movlw b'01010101'
Salida
	movwf PORTB
	goto Principal
	END