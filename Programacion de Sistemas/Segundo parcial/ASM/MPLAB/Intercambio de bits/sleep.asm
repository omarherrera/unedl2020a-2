;Zona de datos 
	LIST P = 16F84A
	INCLUDE <P16F84A.INC>

;Zona de codigo
	ORG 0
	
Inicio
	bsf STATUS,RP0
	clrf PORTB
	movlw b'00011111'
	movwf PORTA
	bcf STATUS,RP0
Principal
	movf PORTA,W
	movwf PORTB
	sleep Principal
	END