;Hola mundo en MPLAB

;Zona datos
LIST P= 16F84A	;Seleccionar/indicar el tipo de PIC
INCLUDE <P16F84A.INC>	;Declaracion para incluir el archivo .INC

;Zona de codigo
	ORG 0	;Idicar que el programa comience en la direccion 0, de la memoria
;Configuracion de puertos
;El bit 5 = RP0 del registro STATUS, Register Bank Bit,
;Si RP0 = 0, se esta seleccionando el banco 0
;Si RP0 = 1, se esta seleccionando el banco 1
	bsf STATUS,RP0;Establece en 1 el bit 5(RP0) del registro STATUS
	
;Permite seleccionar el puerto 1
;Los puertos se configuran en el banco 1

;Si PORTA(TRISA)/PORTB(TRISB) = 0, se configura como salida
;Si PORTA/PORTB = 0, se configura como entrada
	clrf PORTB ;configura como salida el PORTA/PORTB estableciendo en 0 todos sus bits
	bcf STATUS,RP0	;Establecer en 0 el bit 5(RP0) del registro STATUS y Selecciona el banco 0
	bsf PORTB,0	;bit 0 = RB0
;Enviar 1 es una se�al activa para encender el led
	END