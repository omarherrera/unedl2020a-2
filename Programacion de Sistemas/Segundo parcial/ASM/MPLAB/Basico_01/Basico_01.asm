;Mostrar en la barra de leds conectada al puerto B una constante b'01010101'
LIST P = 16F84A
INCLUDE <P16F84A.INC>

CONSTANTE EQU b'01010101'

;codigo
	ORG 0
Inicio
	;Accedemos al banco 1 para configurar entradas y salidas
	bsf STATUS,RP0	;Establece en 1 el bit RP0 de STATUS
	clrf PORTB	;Configura el puerto B como salida
	bcf STATUS,RP0	;Accedemos al banco 0
	movlw CONSTANTE	;Carga constante al registro w
Principal
	movwf PORTB	;Carga lo que hay en w a PORTB
	goto Principal
	END