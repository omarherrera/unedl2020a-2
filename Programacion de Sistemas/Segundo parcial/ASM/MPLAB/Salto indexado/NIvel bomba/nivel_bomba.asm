	LIST P = 16F84A
	INCLUDE<P16F84A.INC>

	ORG 0
Inicio
	clrf PORTB
	bsf STATUS,RP0
	clrf PORTB
	movlw b'00011111'
	movwf PORTA
	bcf STATUS,RP0
Principal
	movf PORTA,W
	andlw b'00000111'
	addwf PCL,F
	goto Configuracion0
	goto Configuracion1
	goto Configuracion2
	goto Configuracion3
	goto Configuracion4
	goto Configuracion5
	goto Configuracion6
	goto Configuracion7
Configuracion0
	movlw b'01100001'
	goto Salida
Configuracion1
	movlw b'01100010'
	goto Salida
Configuracion2
	movlw b'00010000'
	goto Salida
Configuracion3
	movlw b'00100100'
	goto Salida
Configuracion4
	movlw b'00010000'
	goto Salida
Configuracion5
	movlw b'00010000'
	goto Salida
Configuracion6
	movlw b'00010000'
	goto Salida
Configuracion7
	movlw b'00001000'
	goto Salida
Salida
	movwf PORTB
	goto Principal
	END
