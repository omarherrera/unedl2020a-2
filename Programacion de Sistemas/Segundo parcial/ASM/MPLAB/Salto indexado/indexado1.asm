;Declara tabla de verdad con 3 entradas y 6 salidas
;Salto indexado 

;PCL - Registro base + Registro indice (w) 
;	0   0   0   |   0    0    1    0    1    0	; (Configuración 0).
;	0   0   1   |   0    0    1    0    0    1	; (Configuración 1).
;	0   1   0   |   1    0    0    0    1    1	; (Configuración 2).
;	0   1   1   |   0    0    1    1    1    1	; (Configuración 3).
;	1   0   0   |   1    0    0    0    0    0	; (Configuración 4).
;	1   0   1   |   0    0    0    1    1    1	; (Configuración 5).
;	1   1   0   |   0    1    0    1    1    1	; (Configuración 6).
;	1   1   1   |   1    1    1    1    1    1	; (Configuración 7).

LIST P = 16F84A
INCLUDE<P16F84A.INC>

	ORG 0

Inicio
	bsf STATUS,RP0
	clrf PORTB
	movlw b'00011111'
	movwf PORTA
	bcf STATUS,RP0
Principal
	movf PORTA,W
	andlw b'00000111'
	addwf PCL,F
Tabla
	goto Configuracion0
	goto Configuracion1
	goto Configuracion2
	goto Configuracion3
	goto Configuracion4
	goto Configuracion5
	goto Configuracion6
	goto Configuracion7
Configuracion0
	movlw b'00001010'
	goto Salida
Configuracion1
	movlw b'00001001'
	goto Salida
Configuracion2
	movlw b'00100011'
	goto Salida
Configuracion3
	movlw b'00001111'
	goto Salida
Configuracion4
	movlw b'00100000'
	goto Salida
Configuracion5
	movlw b'00000111'
	goto Salida
Configuracion6
	movlw b'00010111'
	goto Salida
Configuracion7
	movlw b'00111111'
	goto Salida
Salida
	movwf PORTB
	goto Principal
	
	END