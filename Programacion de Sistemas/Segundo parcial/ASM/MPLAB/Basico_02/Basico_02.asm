;Mostrar por el PORTB, el dato enviado por el PORTA. Ejemplo "---11001".
LIST P=16F84A
INCLUDE <P16F84A.INC>

;Zona de codigo
	ORG 0
Inicio
	bsf STATUS, RP0 	;Acceso al banco 1
	clrf PORTB		;Configurar PORTB como salida
	movlw b'11111111' 	;Cargamos el numero en w
	movwf PORTA 	;Configuramos PORTA como entrada
	bcf STATUS,RP0		;Regresamos al banco 0
Principal
	movf PORTA,W	;Lee el PORTA y lo envia a W
	movwf PORTB
	goto Principal
	END