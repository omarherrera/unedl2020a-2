;Zona de datos
	LIST P = 16F84A
	INCLUDE <P16F84A.INC>

;Zona de codigo
	ORG 0
Inicio
	bsf STATUS,RP0		;bsf Envia 1
	clrf PORTB
	movlw b'11111111'
	movwf PORTA
	bcf STATUS,RP0
Principal
	comf PORTA,W
	movwf PORTB
	goto Principal
	END