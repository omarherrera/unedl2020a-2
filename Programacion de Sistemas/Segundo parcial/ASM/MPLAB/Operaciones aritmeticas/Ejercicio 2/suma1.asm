;Sumar una constante a lo que se reciba del puerto A
;Zona de datos
LIST P = 16F84A
INCLUDE <P16F84A.INC>

CONSTANTE EQU d'74'		;Declaracion de constante en decimal

;codigo
	ORG 0
Inicio
	bsf STATUS,RP0		;Establece en 1 el bit 5 de STATUS, acceso al banco 0
	clrf PORTB			;El PORTB como salida
	movlw b'11111111' 	;El registro w contiene b'11111'
	movwf PORTA			;Establecemos el puerto A como entrada
	bcf STATUS,RP0			;establece en 0 el bit 5 de STATUS, acceso al banco 0
Principal
	movf PORTA,W		;Carga el registro W con los datos del puerto A
	addlw CONSTANTE		;Sumamos la constante al registro W (W) = PORTA + CONSTANTE
	movwf PORTB 		; Deposita en PORTB el contenido de W
	goto Principal
	END 