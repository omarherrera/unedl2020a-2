;Zona de datos
	LIST P = 16F84A
	INCLUDE <P16F84A.INC>
MASCARA EQU b'01010101'		;Mascara de bits pares siempre '1'

;Zona de codigo
	ORG 0
Inicio
	bsf STATUS,RP0		;Acceso al banco 1
	clrf PORTB		;Configurando como salida PORTB
	movlw b'11111111'
	movwf PORTA		;Configurando el PORTA como entrada
	bcf STATUS,RP0		;Acceso al banco 0
Principal
	movf PORTA,W		;Carga el registro W con los datos de PORTA
	iorlw MASCARA
	movwf PORTB
	goto Principal
	END