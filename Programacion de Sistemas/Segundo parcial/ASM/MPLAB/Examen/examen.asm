; En el PORTB que es salida, va a ser controlado por el bit 1 del PORTA
;Si el bit 1 del PORTA es 1, todos los leds encienden
;Si el bit 1 es igual a 0, entonces solo se encienden los pares

;Zona de datos	
	LIST P = 16F84A
	INCLUDE <P16F84A.INC>

	ORG 0

;Zona de codigo
Inicio
	bsf STATUS,RP0
	clrf PORTB
	movlw b'00011111'
	movwf PORTA
	bcf STATUS,RP0

Principal
	movlw b'11111111'		;supone que el bit 0 es 1, por tanto se encienden todos los leds
	btfss PORTA,1		;�Bit 0 del PORTA, es 1?
	movlw b'01010101'
	movwf PORTB
	goto Principal
	END