LIST P = 16F84A
INCLUDE <P16F84A.INC>

CBLOCK 0x0C
ENDC

NUMERO EQU d'124'

	ORG 0

Inicio
	bsf STATUS,RP0
	clrf PORTB
	bcf STATUS,RP0
Principal
	movlw NUMERO
	call CON_BCD
	movwf PORTB
	goto $
	
	CBLOCK
	Centenas
	Decenas
	Unidades
	ENDC

CON_BCD
	clrf Centenas
	clrf Decenas
	movwf Unidades
Resta_Unidades
	movlw .10
	subwf Unidades,W
	btfss STATUS,C
	goto BIN_BCD_Fin
Incrementar_Decenas
	movwf Unidades
	incf Decenas,F
	movlw .10
	subwf Decenas,W
	btfss STATUS,C
	goto Resta_Unidades
Incrementar_Centenas
	clrf Decenas
	incf Centenas,F
	goto Resta_Unidades
BIN_BCD_Fin
	swapf	Decenas,W
	addwf	Unidades,W
	return
	
	END