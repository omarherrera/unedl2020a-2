%macro escribir 2
mov eax, 4
mov ebx, 1
mov ecx, %1
mov edx, %2
int 0x80
%endmacro

section .data
msg db "Ingrese el valor de num 1: ",0xA
len equ $ - msg

msg2 db "Ingrese el valor del num 2: ",0xA
len2 equ $ - msg2

msg3 db "El numero 1 es mayor."
len3 equ $ - msg3

msg4 db "El numero 2 es mayor."
len4 equ $ - msg4

msg5 db "Los numeros son iguales."
len5 equ $ - msg5

salto db "",0xA
lenSalto equ $ - salto


section .bss
num1 resb 2
num2 resb 2

section .text

leer:
	mov eax, 3
	mov ebx, 0
	mov edx, 2
	int 0x80
ret

    global _start

_start:

    escribir msg,len
    mov ecx, num1
    call leer
    
    escribir salto,lenSalto
    
    escribir msg2,len2
    mov ecx,num2
    call leer
    escribir salto,lenSalto
    
    mov al,[num1]
    sub al,'0'
    mov bl,[num2]
    sub bl,'0'
    
    cmp al,bl
    ja mayor
    cmp al,bl
    jb menor
    cmp al,bl
    je igual
    
mayor:
    escribir msg3,len3
    jmp salida

menor:
    escribir msg4,len4
    jmp salida


igual:
    escribir msg5,len5
    jmp salida

    
salida:
    mov eax,1
    int 0x80