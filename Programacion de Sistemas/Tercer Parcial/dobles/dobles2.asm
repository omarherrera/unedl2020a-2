LIST P=16F84A
INCLUDE <P16F84A.INC>

	ORG 0
Inicio
	bsf STATUS,RP0
	clrf PORTB
	movwf PORTA
	bcf STATUS,RP0
Principal
	movf PORTA,W	;Lee el PORTA y lo envia a W
	movwf PORTB
	goto Principal

	END