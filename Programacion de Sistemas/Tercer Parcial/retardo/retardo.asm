;LIST P=16F84A
INCLUDE <P16F84A.INC>

CBLOCK 0x0C
tiempo
ENDC

	ORG 0
Inicio
	bsf STATUS,RP0
	clrf PORTB
	bcf STATUS,RP0
Principal
	bsf PORTB,0
	bcf PORTB,1
	bcf PORTB,2
	call Retardo_1s
	bcf PORTB,0
	bsf PORTB,1
	bcf PORTB,2
	call Retardo_1s
	bcf PORTB,0
	bcf PORTB,1
	bsf PORTB,2
	call Retardo_1s
	goto Principal

;Subrutina
	
	INCLUDE<RETARDOS.INC>
	
	END