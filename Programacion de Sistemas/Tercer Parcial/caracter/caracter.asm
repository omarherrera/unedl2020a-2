LIST P=16F84A
INCLUDE <P16F84A.INC>

	CBLOCK 0x0C
	ENDC

CARACTER EQU 'P'

	ORG 0

Inicio
	bsf STATUS,RP0
	clrf PORTB
	bcf STATUS,RP0
Principal
	movlw CARACTER
	call Letra7S
	movwf PORTB
	goto Principal

	CBLOCK
	Dato
	ENDC

Letra7S
	movwf Dato
	movlw 'A'
	subwf Dato,W
	addwf PCL,F
	DT 77h, 7Ch, 39h, 5Eh, 79h, 71h, 6Fh, 76h, 19h, 1Eh, 7Ah, 38h, 37h
	DT 54h, 3Fh, 73h, 67h, 50h, 6Dh, 78h, 1Ch, 3Eh, 1Dh, 70h, 6Eh, 49h

	END