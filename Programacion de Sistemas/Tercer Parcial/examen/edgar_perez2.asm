LIST P=16F84A
INCLUDE <P16F84A.INC>
	
CBLOCK 0X0C	
ENDC

	ORG 0
Inicio
	bsf		STATUS,RP0
	clrf	PORTB
	movlw	b'00000111'
	movwf	PORTA
	bcf		STATUS,RP0
Principal
	movlw	b'00000100'
	btfsc	PORTA,2
	call	PrendeVerde
	movwf	PORTB
	goto 	Principal
	
PrendeRojo
	movlw	b'00000100'
	movwf	PORTB
PrendeVerde
	movlw	b'00000001'
	movwf	PORTB
	call	Retardo_2s
	movlw	b'00000010'
	movwf	PORTB
	call 	Retardo_2s
		
Salida 
	INCLUDE<RETARDOS.INC>
	END