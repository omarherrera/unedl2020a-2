	LIST P = 16F84A
	INCLUDE<P16F84A.INC> 		;Establece el PIC a usar

	ORG 0
Inicio
	clrf PORTB 					;Limpia el PORTB
	bsf STATUS,RP0 				;Establece el PORTB como salida
	clrf PORTB
	movlw b'00001111' 			;Establece el numero de entradas del PORTA 
	movwf PORTA					;Manda esa configuración al PORTA
	bcf STATUS,RP0				;Establece el PORTA como entrada
Principal
	movf PORTA,W				;Mueve los datos al PORTA			
	call PrenderLed
	movwf PORTB
	goto Principal

;Subrutina
PrenderLed
	addwf PCL,F
InicioTabla
	DT 77h,7Ch,39h,5Eh,79h,71h,6Fh,76h,19h,1Eh,7Ah,38h,37h,54h,3Fh
	DT 73h,67h,50h,60h,78h,1Ch,3Eh,1Dh,70h,6Eh,49h
FinTabla
	
	END