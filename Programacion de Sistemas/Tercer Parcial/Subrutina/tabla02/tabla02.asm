LIST P=16F84A
INCLUDE <P16F84A.INC>

	ORG 0
Inicio
	bsf STATUS,RP0
	clrf PORTB
	movlw b'00011111'
	movwf PORTA
	bcf STATUS,RP0
Principal
	movf PORTA,W
	andlw b'00000111'
	call PrenderLed
	movwf PORTB
	goto Principal
	
;Subrutina PrenderLeds
PrenderLed		;Nombre de la subrutina para acceder a ella
	addwf PCL,F		;Contador de programa
	retlw b'00001010'
	retlw b'00001001'
	retlw b'00100011'
	retlw b'00001111'
	retlw b'00100000'
	retlw b'00000111'
	retlw b'00010111'
	retlw b'00111111'

	END