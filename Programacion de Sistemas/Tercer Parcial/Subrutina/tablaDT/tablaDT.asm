LIST P=16F84A
INCLUDE <P16F84A.INC>

	ORG 0
Inicio
	bsf STATUS,RP0
	clrf PORTB
	movlw b'00011111'
	bcf STATUS,RP0
Principal
	movf PORTA,W
	sublw d'8'
	btfss STATUS,C
	goto Pares
	movf PORTA,W
	call PrenderLed
	goto Salida
	
;Subrutina PrenderLeds
PrenderLed		;Nombre de la subrutina para acceder a ella
	addwf PCL,F		;Contador de programa
	DT 0x00, 0x01, 0x03, 0x03, 0x07, 0x0F, 0x1F, 0x3F, 0X7F

Pares
	movlw b'01010101'
Salida
	movwf PORTB
	goto Principal

	END