	LIST P = 16F84A
	INCLUDE<P16F84A.INC> 		;Establece el PIC a usar

	ORG 0
Inicio
	clrf PORTB 					;Limpia el PORTB
	bsf STATUS,RP0 				;Establece el PORTB como salida
	clrf PORTB
	movlw b'00001111' 			;Establece el numero de entradas del PORTA 
	movwf PORTA					;Manda esa configuración al PORTA
	bcf STATUS,RP0				;Establece el PORTA como entrada
Principal
	movf PORTA,W				;Mueve los datos al PORTA
	andlw b'00001111';
	call PrenderLed
	movwf PORTB							;Muestra en el PORTB los datos de salida
	goto Principal

;Subrutina
PrenderLed
	addwf PCL,F
Tabla
	retlw b'00000001'
	retlw b'00000011'
	retlw b'00000100'
	retlw b'00111111'
	retlw b'00001100'
	retlw b'00111111'
	retlw b'00111111'
	retlw b'00111111'
	retlw b'00110000'
	retlw b'00111111'
	retlw b'00111111'
	retlw b'00111111'
	retlw b'00111111'
	retlw b'00111111'
	retlw b'00111111'
	retlw b'00111111'

	END