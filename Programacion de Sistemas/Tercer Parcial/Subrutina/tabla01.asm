LIST P=16F84A
INCLUDE <P16F84A.INC>

	ORG 0
Inicio
	bsf STATUS,RP0
	clrf PORTB
	movlw b'00011111'
	bcf STATUS,RP0
Principal
	movf PORTA,W
	sublw d'8'
	btfss STATUS,C
	goto Pares
	movf PORTA,W
	call PrenderLed
	goto Salida
	
;Subrutina PrenderLeds
PrenderLed		;Nombre de la subrutina para acceder a ella
	addwf PCL,F		;Contador de programa
Tabla
	retlw b'00000000'
	retlw b'00000001'
	retlw b'00000011'
	retlw b'00000111'
	retlw b'00001111'
	retlw b'00011111'
	retlw b'00111111'
	retlw b'01111111'

Pares
	movlw b'01010101'
Salida
	movwf PORTB
	goto Principal

	END