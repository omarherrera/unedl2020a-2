LIST P=16F84A
INCLUDE <P16F84A.INC>

	CBLOCK 0x0C
	ENDC

	ORG 0
Inicio
	bsf STATUS,RP0
	clrf PORTB
	movlw b'00011111'
	movwf PORTA
	bcf STATUS,RP0
Principal
	movlw (MensajeFin-MensajeInicio) 	;Encuentra la longitud mensaje
	;El numero que ingreso PORTA no sea Mayor a la longitud	
	subwf PORTA,W
	btfsc STATUS,C
	goto MensajeMenor
	movf PORTA,W
	call LeerCaracter
	call ASCII_a_7Segmentos
	goto Salida

Salida
	movwf PORTB
	goto Principal

MensajeMenor
	movlw b'10000000'		;El caracter fuera de rango
;Subrutina
LeerCaracter
	addwf PCL,F
MensajeInicio
	DT "OMAR PEREZ"
MensajeFin

	INCLUDE <DISPLAY_7S.INC>

	END