section .data
msg db "La suma de 5 y 2 es:",0xA,0xD
len equ $ - msg ;extrae la longitud de msg
salto db "",0xa,0xd
lensalto equ $ - salto

;seccion de variables no declaradas
section .bss
;declarar una ubicacion de memoria que no esta
;inicializada por medio de una etiqueta
res resb 1
;reserva espacio en la memoria para poder colocar un resultado
;reserva 1 byte

section .text
 	global _start

_start:

	mov eax,4
	mov ebx,1
	mov ecx,msg
	mov edx,len
	int 0x80

;inicializar los registros con los valores
	mov eax,5
	mov ebx,2
;realizamos la operacion
	add eax,ebx ;eax = eax + ebx
	
	add eax,'0'; agregar un espacio y convertir en cadena
	mov [res],eax

;salida de resultado
	mov eax,4
	mov ebx,1
	mov ecx,res
	mov edx,1
	int 0x80

;salir
	mov eax,1 ;sys_exit
	int 0x80