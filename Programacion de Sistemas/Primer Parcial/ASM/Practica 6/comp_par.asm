SYS_EXIT equ 1
SYS_READ equ 3
SYS_WRITE equ 4
STDOUT equ 1
STDIN equ 0

section .data
msg db "Es par",0xa,0xd
len equ $ - msg
msg2 db "Es impar",0xa,0xd
len2 equ $ - msg2
msg3 db "Ingrese el primer numero: ",0xa,0xd
len3 equ $ - msg3

section .bss
num1 resb 1

section .text
	global _start

_start:
;impresion 1
	mov eax,SYS_WRITE
	mov ebx,STDOUT
	mov ecx,msg3
	mov edx,len3
	int 0x80

;lectura 1
	mov eax,SYS_READ
	mov ebx,STDIN
	mov ecx,num1
	mov edx,1
	int 0x80

;convertir de ascii a decimal
	mov eax,[num1]
	sub eax,'0'

	cmp eax,2
	jz impresion

	mov eax,SYS_WRITE
	mov ebx,STDOUT
	mov ecx,msg2
	mov edx,len2
	int 0x80
	jmp salida

impresion:
	mov eax,SYS_WRITE
	mov ebx,STDOUT
	mov ecx,msg
	mov edx,len
	int 0x80

salida:
	mov eax,SYS_EXIT
	int 0x80 