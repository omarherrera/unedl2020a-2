SYS_EXIT equ 1
SYS_READ equ 3
SYS_WRITE equ 4
STDOUT equ 1
STDIN equ 0

section .data
msg db "Ingrese el primer numero: "
len equ $ - msg
msg2 db "Ingrese el segundo numero: "
len2 equ $ - msg2
msg3 db "El resultado es: "
len3 equ $ - msg3
salto db "",0xa,0xd
lensalto equ $ - salto

section .bss
num1 resb 2
num2 resb 2
res resb 1

section .text
	global _start

_start:
	
;mensaje 1
	mov eax,SYS_WRITE
	mov ebx,STDOUT
	mov ecx,msg
	mov edx,len
	int 0x80

;lectura 1
	mov eax,SYS_READ
	mov ebx,STDIN
	mov ecx,num1
	mov edx,2
	int 0x80

;mensaje 2
	mov eax,SYS_WRITE
	mov ebx,STDOUT
	mov ecx,msg2
	mov edx,len2
	int 0x80

;lectura 2
	mov eax,SYS_READ
	mov ebx,STDIN
	mov ecx,num2
	mov edx,2
	int 0x80

;convertir ascii a decimal
	mov eax,[num1]
	sub eax,'0'

	mov ebx,[num2]
	sub ebx,'0'

	add eax,ebx

	add eax,'0'
	mov [res],eax

;mensaje 3
	mov eax,SYS_WRITE
	mov ebx,STDOUT
	mov ecx,msg3
	mov edx,len3
	int 0x80

;resultado
	mov eax,SYS_WRITE
	mov ebx,STDOUT
	mov ecx,res
	mov edx,1
	int 0x80

	mov eax,SYS_EXIT
	int 0x80 