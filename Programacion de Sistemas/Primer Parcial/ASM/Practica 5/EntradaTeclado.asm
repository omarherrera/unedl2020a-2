section .data
msg db "Ingrese un dato :",0xA,0xD
len equ $ - msg
msg2 db "Su nombre es :",0xA,0xD
len2 equ $ - msg2


section .bss
data resb 4

section .text
	global _start

_start:

;mostrar el mensaje en pantalla
	mov eax,4
	mov ebx,1
	mov ecx,msg
	mov edx,len
	int 0x80

;leer datos
	mov eax,3
	mov ebx,0
	mov ecx,data
	mov edx,4
	int 0x80

;imprimir el dato introducido
	mov eax,4
	mov ebx,1
	mov ecx,msg2
	mov edx,len2
	int 0x80

	mov eax,4
	mov ebx,1
	mov ecx,data
	mov edx,4
	int 0x80

	mov eax,1
	int 0x80
