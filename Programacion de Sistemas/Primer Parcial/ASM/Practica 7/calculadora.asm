section .data
msg db "Ingresa el numero 1: ",0xa,0xd
len equ $ - msg
msg2 db "Ingresa el numero 2: ",0xa,0xd
len2 equ $ - msg2
msg3 db "SUMA",0xa,0xd
len3 equ $ - msg3
msg4 db "RESTA",0xa,0xd
len4 equ $ - msg4
msg5 db "MULTIPLICACION",0xa,0xd
len5 equ $ - msg5
msg6 db "DIVISION",0xa,0xd
len6 equ $ - msg6
msg7 db "Opcion: ",0xa,0xd
len7 equ $ - msg7
msg8 db "El resultado es: ",0xa,0xd
len8 equ $ - msg8
msg9 db "OPCION INVLIDA",0xa,0xd
len9 equ $ - msg9
msg10 db "--CALCULADORA--",0xa,0xd
len10 equ $ - msg10

section .bss
num1 resb 2
num2 resb 2
opc resb 2
res resb 2

section .text
	global _start

_start:

;mesaje y lectura primer numero
	mov eax,4
	mov ebx,1
	mov ecx,msg
	mov edx,len
	int 0x80

	mov eax,3
	mov ebx,0
	mov ecx,num1
	mov edx,2
	int 0x80

;mesaje y lectura segundo numero
	mov eax,4
	mov ebx,1
	mov ecx,msg2
	mov edx,len2
	int 0x80

	mov eax,3
	mov ebx,0
	mov ecx,num2
	mov edx,2
	int 0x80

;mesajes de suma resta mul y div
	mov eax,4
	mov ebx,1
	mov ecx,msg3
	mov edx,len3
	int 0x80

	mov eax,4
	mov ebx,1
	mov ecx,msg4
	mov edx,len4
	int 0x80

	mov eax,4
	mov ebx,1
	mov ecx,msg5
	mov edx,len5
	int 0x80

	mov eax,4
	mov ebx,1
	mov ecx,msg6
	mov edx,len6
	int 0x80

;mensaje de opcion
	mov eax,4
	mov ebx,1
	mov ecx,msg7
	mov edx,len7
	int 0x80

	mov eax,3
	mov ebx,0
	mov ecx,opc
	mov edx,2
	int 0x80

	mov ah,[opc]
	sub ah,'0'

;comparacion y salto a suma
	cmp ah,1
	je suma

	cmp ah,2
	je resta

;mesaje de error
	mov eax,4
	mov ebx,1
	mov ecx,msg9
	mov edx,len10
	int 0x80

	jmp salida

suma:
	mov al,[num1]
	mov bl,[num2]

	sub al,'0'
	sub bl,'0'

	add al,bl
	add al,'0'
	mov [res],al

	mov eax,4
	mov ebx,1
	mov ecx,msg8
	mov edx,len8
	int 0x80

	mov eax,4
	mov ebx,1
	mov ecx,res
	mov edx,2
	int 0x80

	jmp salida

resta:
	mov al,[num1]
	mov bl,[num2]

	sub al,'0'
	sub bl,'0'

	sub al,bl
	add al,'0'
	mov [res],al

	mov eax,4
	mov ebx,1
	mov ecx,msg8
	mov edx,len8
	int 0x80

	mov eax,4
	mov ebx,1
	mov ecx,res
	mov edx,2
	int 0x80

	jmp salida


salida:
	mov eax,1
	int 0x80