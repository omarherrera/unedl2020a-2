section .data ;segmento de datos declarados
msg db "Hola mundo",0xa,0xd ;guardamos en msg el valor hola mundo
len equ $-msg ;obtenemos la longitud del mensaje

section .text ;segmento de instrucciones
	global _start

_start: ;indica el inicio de instrucciones

mov eax,4 ;sys_write
mov ebx,1 ;stdout
mov ecx,msg ;mensaje que se quiere imprimir
mov edx,len ;longitud del mensaje 
int 0x80 ;llamada a la intterrupcion (0x21)

mov eax,1 ;sys_exit
int 0x80 ;llamada a la interrupcion

;nasm -f elf hola.asm
;ld -m elf_i386 -s -o hola hola.o
;./hola