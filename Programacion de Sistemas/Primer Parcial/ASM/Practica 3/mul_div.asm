section .data
msg db "Multiplicacion de 3*3:"
len equ $ - msg
msg2 db "Multiplicacion de (-3)*(-3):"
len2 equ $ -msg2
msg3 db "Division de 6/3:"
len3 equ $ -msg3
msg4 db "Division de (-6)/(-3):"
len4 equ $ -msg4
salto db "",0xa,0xd
lensalto equ $ - salto

section .bss
res resb 1

section .text
	global _start

_start:

;operacion 1
	mov eax,4
	mov ebx,1
	mov ecx,msg
	mov edx,len
	int 0x80

	mov eax,3
	mov ebx,3

	mul ebx

	add eax,'0'
	mov [res],eax

	mov eax,4
	mov ebx,1
	mov ecx,res
	mov edx,1
	int 0x80

	mov eax,4 ;sys_exit
	mov ebx,1
	mov ecx,salto
	mov edx,lensalto
	int 0x80

;operacion 2
	mov eax,4
	mov ebx,1
	mov ecx,msg2
	mov edx,len2
	int 0x80

	mov eax,-3
	mov ebx,-3

	imul ebx

	add eax,'0'
	mov [res],eax

	mov eax,4
	mov ebx,1
	mov ecx,res
	mov edx,1
	int 0x80

	mov eax,4 ;sys_exit
	mov ebx,1
	mov ecx,salto
	mov edx,lensalto
	int 0x80

;operacion 3
	mov eax,4
	mov ebx,1
	mov ecx,msg3
	mov edx,len3
	int 0x80

	mov eax,6
	mov ebx,3
	mov edx,0

	div ebx

	add eax,'0'
	mov [res],eax

	mov eax,4
	mov ebx,1
	mov ecx,res
	mov edx,1
	int 0x80

	mov eax,4 ;sys_exit
	mov ebx,1
	mov ecx,salto
	mov edx,lensalto
	int 0x80

;operacion 4

	mov eax,4
	mov ebx,1
	mov ecx,msg4
	mov edx,len4
	int 0x80

	mov eax,6
	mov ebx,3
	mov edx,0

	idiv ebx

	add eax,'0'
	mov [res],eax

	mov eax,4
	mov ebx,1
	mov ecx,res
	mov edx,1
	int 0x80

	mov eax,4 ;sys_exit
	mov ebx,1
	mov ecx,salto
	mov edx,lensalto
	int 0x80

	mov eax,1
	int 0x80