section .data
msg db "Ingrese 5 numeros: "
len equ $ -msg

msg1 db "El mayor es:"
len1 equ $ -msg1

arreglo db 0,0,0,0,0
la equ $ - arreglo

salto db 0xa,0xd
lensalto equ $ - salto

section .bss
res resb 2

section .text
    global _start

_start:

    mov eax,4
    mov ebx,1
    mov ecx,msg
    mov edx,len
    int 0x80
    
    mov esi,arreglo ;enviamos la posicion de inicio de arre
    mov edi,0 ;contador de la posicion
    ;aumentar 1 en ambos para llevar el control
    ;esi para que avance de posicion y edi el contador
    ;leer los datos del teclado
leer:
    mov eax,3
    mov ebx,0
    mov ecx,res
    mov edx,2
    int 0x80
    
    mov al,[res] ;mover el valor a al
    sub al,'0' ;restar el carecter para poderlo evualuar
    
    mov [esi],al ;movemos el valor de al a esi
    add esi,1
    add edi,1
    
    cmp edi,la ;compara edi con la longitud del arreglo
    ;si edi es menor que la 
    jb leer
    
    mov ecx,0
    mov bl,0
    ;bl va a almacenar nuestro numero mas grande
 ciclo:   
    mov al, [arreglo+ecx] ; le suma la direccion de memoria, es decir se desplaza
    ;va a la primera ubicacion de arre y le va aumentar lo que tenga ecx
    ;que en la primera vuelta es 0
    cmp al, bl
    jb reg
    mov bl,al
    
reg:
    inc ecx
    cmp ecx,la
    jb ciclo  
    
imprimir:
    add bl,'0'
    mov [res],bl
    
    mov eax,4
    mov ebx,1
    mov ecx,msg1
    mov edx,len1
    int 0x80

    mov eax,4
    mov ebx,1
    mov ecx,res
    mov edx,2
    int 0x80
    
    mov eax,4
    mov ebx,1
    mov ecx,salto
    mov edx,lensalto
    
salida:
    mov eax,1
    int 0x80