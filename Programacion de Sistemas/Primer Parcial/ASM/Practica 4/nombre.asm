section .data
msg db "Hola "
len equ $-msg

section .bss
letra resb 3

section .text
	global _start

_start:

;hola mundo
	mov eax,4 ;sys_write
	mov ebx,1 ;stdout
	mov ecx,msg
	mov edx,len
	int 0x80

;operacion 1
	mov eax,20
	mov ebx,11
	add eax,ebx

	add eax,'0'
	mov [letra],eax

	mov eax,4
	mov ebx,1
	mov ecx,letra
	mov edx,1
	int 0x80

;operacion 2
	mov eax,30
	mov ebx,31
	add eax,ebx

	add eax,'0'
	mov [letra],eax

	mov eax,4
	mov ebx,1
	mov ecx,letra
	mov edx,1
	int 0x80

;operacion 3

	mov eax,7
	mov ebx,7
	mov edx,0
	mul ebx

	add eax,'0'
	mov [letra],eax

	mov eax,4
	mov ebx,1
	mov ecx,letra
	mov edx,1
	int 0x80

;operacion 4

	mov eax,132
	mov ebx,2
	div ebx


	add eax,'0'
	mov [letra],eax

	mov eax,4
	mov ebx,1
	mov ecx,letra
	mov edx,1
	int 0x80

	mov eax,1
	int 0x80